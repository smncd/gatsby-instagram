import React from 'react';
import { graphql } from 'gatsby';
import Posts from '../components/Posts';
import styled from 'styled-components';

const Wrapper = styled.div`
  font-family: sans-serif;
  max-width: 1280px;
  margin: 4rem auto auto auto;
`;

const Title = styled.h1`
  text-transform: uppercase;
  font-size: 50px;
  margin-bottom: 4rem;
`;

const Index = ({ data }) => {
  return (
    <Wrapper>
      <Title>Instagram GraphQL</Title>
      <Posts source={data.allInstaNode.edges} />
    </Wrapper>
  );
};

export default Index;

export const query = graphql`
  {
    allInstaNode {
      edges {
        node {
          id
          caption
          timestamp
          localFile {
            id
            publicURL
          }
        }
      }
    }
  }
`;
