import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.section`
  display: grid;
  grid-template-columns: repeat(3, 33%);
`;

const Grid = ({ children }) => {
  return <Wrapper>{children}</Wrapper>;
};

export default Grid;
