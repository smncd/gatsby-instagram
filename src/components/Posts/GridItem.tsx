import React from 'react';
import styled from 'styled-components';
import { StaticImage } from 'gatsby-plugin-image';

const Wrapper = styled.article`
  margin: 1rem;
  padding: 2rem 0 2rem 0;
  border-radius: 1rem;
  box-shadow: 0 0 20px #0006;
`;

const Image = styled.img`
  width: 100%;
  height: auto;
`;

const Caption = styled.p`
  padding: 0.5em;
  font-size: 14px;
`;

const GridItem = ({ post }) => {
  return (
    <Wrapper
      id={`___post---${post.node.id}`}
      className={`post---${post.node.id}`}
    >
      <Image src={post.node.localFile.publicURL} alt={post.node.id} />
      <Caption>{post.node.caption}</Caption>
    </Wrapper>
  );
};

export default GridItem;
