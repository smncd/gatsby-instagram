import React from 'react';
import Grid from './Grid';
import GridItem from './GridItem';

const Posts = ({ source }) => {
  const allPosts = source.map((node: object) => {
    return <GridItem post={node} />;
  });

  return <Grid>{allPosts}</Grid>;
};

export default Posts;
